#include "Person.h"
#include <iostream>

int main() {
	const Person p_correct      = { "292790001", {"Abdula", "Ibn", "Khattab"}, {1980, 2, 29}, Sex::Male };
    const Person p_incorrect1   = { "328730000", {"Leila", "Zulfia", "Gulchatai"}, {1990, 1, 2}, Sex::Female };
    const Person p_incorrect2   = { "292790", {"Abdula", "Ibn", "Khattab"}, {1980, 2, 29}, Sex::Male };
    const Person p_incorrect3   = { "123", {"Abdula", "Ibn", "Khattab"}, {1980, 2, 29}, Sex::Male };

    Validator validator;
	bool res;
    
    res = validator.ValidatePerson(p_correct);
    std::cout << "Step 1. Result: " << ((res)?"Success.":"Fault.") << std::endl;
    
    res = validator.ValidatePerson(p_incorrect1);
    std::cout << "Step 2. Result: " << ((res)?"Success.":"Fault.") << std::endl;
    
    std::cout << "Step 3. Result: ";
    try {
        res = validator.ValidatePerson(p_incorrect2);
        std::cout << ((res)?"Success.":"Fault.") << std::endl;
    } catch (const CustomException & e) {
        e.Show();
    }
    
    std::cout << "Step 4. Result: ";
    try {
        res = validator.ValidatePerson(p_incorrect3);
        std::cout << ((res)?"Success.":"Fault.") << std::endl;
    } catch (const CustomException & e) {
        e.Show();
    }
    
    return 0;
}
