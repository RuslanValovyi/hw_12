homework task:

Design and implement your own exceptions hierarchy for the [Person library]
(https://github.com/SergiiPiatakov/ExceptionSnippets/tree/master/Person). 
Add a `main` function which demonstrates exception handling.

Hints:
- try to find functions which may cause illegal memory access with invalid input data;
- add input parameters validations for those functions;
- throw a specific exception if input data is invalid;
- this is creative task so feel free to use your imagination.

Please, provide me with a pull request which includes your exceptions hierarchy and other necessary changes.

In case of any difficulties don't hesitate to ask for help.

Best Regards,
Sergii.
